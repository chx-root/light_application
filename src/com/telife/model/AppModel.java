package com.telife.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * @author york
 * @since 2020/6/9
 */
public class AppModel {

    private final SimpleStringProperty imgUrl;
    private final SimpleStringProperty appName;
    private final SimpleStringProperty appDesc;
    private final SimpleStringProperty appUrl;


    public AppModel(String imgUrl, String appName, String appDesc, String appUrl) {
        this.imgUrl = new SimpleStringProperty(imgUrl);
        this.appName = new SimpleStringProperty(appName);
        this.appDesc = new SimpleStringProperty(appDesc);
        this.appUrl = new SimpleStringProperty(appUrl);
    }

    public String getImgUrl() {
        return imgUrl.get();
    }

    public SimpleStringProperty imgUrlProperty() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl.set(imgUrl);
    }

    public String getAppName() {
        return appName.get();
    }

    public SimpleStringProperty appNameProperty() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName.set(appName);
    }

    public String getAppDesc() {
        return appDesc.get();
    }

    public SimpleStringProperty appDescProperty() {
        return appDesc;
    }

    public void setAppDesc(String appDesc) {
        this.appDesc.set(appDesc);
    }

    public String getAppUrl() {
        return appUrl.get();
    }

    public SimpleStringProperty appUrlProperty() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl.set(appUrl);
    }

    @Override
    public String toString() {
        return "AppModel{" +
                "imgUrl=" + imgUrl +
                ", appName=" + appName +
                ", appDesc=" + appDesc +
                ", appUrl=" + appUrl +
                '}';
    }
}
