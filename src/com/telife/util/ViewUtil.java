package com.telife.util;

import com.telife.constant.CommonConstant;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.image.Image;

import java.io.File;
import java.net.URL;

/**
 * @author york
 * @since 2020/6/9
 */
public class ViewUtil {

    /**
     * 设置组件是否可用
     * @param disable (true表示不可用)
     * */
    public static void setViewDisable(boolean disable, Node ...nodes){
        Platform.runLater(()->{
            for (Node node : nodes) {
                node.setDisable(disable);
            }
        });
    }

    /**
     * 设置图像，如果图像损坏，则使用缺省图
     * @param imageName 图片名称
     * @return
     */
    public static Image setImage(String imageName){
        File imageFile = new File(CommonConstant.APP_IMAGE_FILE + imageName);
        Image image = new Image(imageFile.toURI().toString());
        //检测图像是否损坏
        if(image.isError()){
            URL url = ViewUtil.class.getClassLoader().getResource("resources/icon/img_broken.png");
            image = new Image(url.toExternalForm());
        }
        return image;
    }

}
