package com.telife.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.*;
import cn.hutool.extra.ssh.JschUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.sun.imageio.plugins.common.ImageUtil;
import com.telife.constant.CommonConstant;
import com.telife.entity.App;
import com.telife.model.AppModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author york
 * @since 2020/6/9
 */
public class CommonUtil {

    /**
     * 实体类转换成模型类
     * @param app
     * @return
     */
    public static AppModel appToAppModel(App app){
        return new AppModel(app.getImgUrl(),app.getAppName(),app.getAppDesc(),app.getAppUrl());
    }


    /**
     * 模型转换成实体类
     * @param appModel
     * @return
     */
    public static App appModelToAPP(AppModel appModel){
        App app = new App();
        app.setAppName(appModel.getAppName());
        app.setAppDesc(appModel.getAppDesc());
        app.setImgUrl(appModel.getImgUrl());
        app.setAppUrl(appModel.getAppUrl());
        return app;
    }

    /**
     * 检验字符串是否为正确的网址格式
     */
    public static boolean matcheUrl(String url){
        String regex = "^([hH][tT]{2}[pP]:/*|[hH][tT]{2}[pP][sS]:/*|[fF][tT][pP]:/*)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+(\\?{0,1}(([A-Za-z0-9-~]+\\={0,1})([A-Za-z0-9-~]*)\\&{0,1})*)$";
        Pattern pattern = Pattern.compile(regex);
        if (pattern.matcher(url).matches()) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 把图片复制到工程指定目录中
     * @param sourceFilePath 源文件地址
     */
    public static String copyImageToProject(String sourceFilePath){
        //源文件地址
        File source = new File(sourceFilePath);
        //获取图片格式
        String suffix = sourceFilePath.substring(sourceFilePath.lastIndexOf("."));
        //获取当前时间戳并且截取时间戳后5位数字
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String subString = timeStamp.substring(timeStamp.length()-5);
        //使用UUID生成新的文件名
        String newImageName = (RandomUtil.randomString(8) + subString)+suffix;
        //图片保存路径
        String imageSavePath = CommonConstant.APP_IMAGE_FILE + newImageName;
        //目标地址
        File dest = new File(imageSavePath);
        //目标地址父目录
        File destParant = dest.getParentFile();
        //如果父目录不存在，则创建目录
        if(!destParant.exists()){
            FileUtil.mkdir(destParant);
        }
        FileUtil.copy(source,dest,true);
        return newImageName;
    }

    /**
     * 删除文件
     * @param targetPath 要删除的目标文件路径
     */
    public static void deleteFile(String targetPath){
        File targetFile = new File(CommonConstant.APP_IMAGE_FILE + targetPath);
        if (targetFile.exists()){
            FileUtil.del(targetFile);
        }
    }

    /**
     * 保存应用程序数据
     * @param jsonData
     */
    public static void saveDataToFile(String jsonData){
        FileUtil.writeString(jsonData, CommonConstant.APP_DATA_FILE_PATH, CharsetUtil.CHARSET_UTF_8);
    }

    /**
     * 读取应用程序数据
     * @return
     */
    public static List<App> loadDataFromFile(){
        List<App> appList = new ArrayList<>();
        File targetFile = new File(CommonConstant.APP_DATA_FILE_PATH);
        //检测目标文件是否存在
        if(targetFile.exists()){
            String jsonStr = FileUtil.readString(targetFile, CharsetUtil.CHARSET_UTF_8);
            JSONArray jsonArray = JSONUtil.parseArray(jsonStr);
            jsonArray.forEach(json->{
                App app = JSONUtil.toBean(json.toString(), App.class);
                appList.add(app);
            });
        }
        return appList;
    }

}
