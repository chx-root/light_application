package com.telife.controller;

import com.telife.component.AboutDialog;
import com.telife.component.LoadAppDialog;
import com.telife.component.ManageAppDialog;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private static BorderPane rootPane;

    @FXML
    private WebView webView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //设置WebView的初始页面
        setDefaultPage();
    }

    /**
     * 获取场景
     */
    public Stage getStage(){
        return (Stage) rootPane.getScene().getWindow();
    }

    /**
     * 设置初始页面
     * @return
     */
    private void setDefaultPage(){
        URL url = getClass().getClassLoader().getResource("resources/html/default.html");
        webView.getEngine().load(url.toExternalForm());
    }



/****************↓↓↓↓↓↓ 以下是各个控件的动作事件 ↓↓↓↓↓↓↓************************************/

    /**
     * 点击菜单"载入文件"时调用的方法
     */
    @FXML
    private void loadApp() {
        LoadAppDialog loadAppDialog = new LoadAppDialog();
        loadAppDialog.showDialog(webView);
    }

    /**
     * 点击菜单"退出"时调用的方法
     */
    @FXML
    private void exit() {
        Platform.exit();
    }


    /**
     * 点击菜单"关于"时调用的方法
     */
    @FXML
    private void about() {
        new AboutDialog().showAndWait();
    }

    /**
     * 点击"管理应用"时调用的方法
     */
    @FXML
    private void manageApp() {
        ManageAppDialog manageAppDialog = new ManageAppDialog();
        manageAppDialog.showDialog();
    }

    /**
     * 点击"页面初始化"时调用的方法
     */
    public void initPage(ActionEvent actionEvent) {
        setDefaultPage();
    }
}
