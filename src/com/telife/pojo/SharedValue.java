package com.telife.pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 共享变量
 * @author york
 * @since 2020/6/10
 */
public class SharedValue {

    /**
     * 是否已经编辑了app表格视图里的列表信息
     */
    public static AtomicBoolean hasEditedAppTableView = new AtomicBoolean(false);

    /**
     * 新的应用程序图片保存路径
     */
    public static List<String> imgSavePaths = Collections.synchronizedList(new ArrayList<>());

    /**
     * 用于保存被替换的应用程序图片（编辑应用信息，如果有更改图片时会用到此集合）
     */
    public static List<String> tempImgPaths = Collections.synchronizedList(new ArrayList<>());


    /**
     * 更改app表格视图的编辑状态
     * @param isEdited
     */
    public static void setAppTableViewEditStatus(boolean isEdited){
        hasEditedAppTableView.compareAndSet(!isEdited,isEdited);
    }


}
