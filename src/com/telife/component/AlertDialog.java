package com.telife.component;

import com.telife.Main;
import javafx.scene.control.Alert;

/**
 * @author york
 * @since 2020/6/10
 */
public class AlertDialog {

    public void showAndWait(Alert.AlertType alertType,String title,String content){
        Alert alert = new Alert(alertType);
        alert.setHeaderText(null);
        alert.initOwner(Main.getStage());
        alert.setTitle(title);
        alert.setContentText(content);
        alert.showAndWait();
    }
}
