package com.telife.component;

import com.telife.Main;
import com.telife.constant.CommonConstant;
import com.telife.controller.MainController;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.net.URL;

/**
 * @author york
 * @since 2020/6/12
 */
public class AboutDialog {

    public void showAndWait(){

        Dialog dialog = new Dialog();
        dialog.setTitle("关于轻应用");
        dialog.setHeaderText(null);

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        dialog.initOwner(Main.getStage());

        //设置按钮
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        //自定义对话框布局
        GridPane gridPane = new GridPane();

        //图标
        ImageView imageView = new ImageView(CommonConstant.LOGO);
        imageView.setFitWidth(150);
        imageView.setFitHeight(150);

        //版本信息
        Label version = new Label(String.format("版本信息：%s", CommonConstant.VERSION));
        GridPane.setMargin(version,new Insets(10,0,0,0));

        //版权声明
        Label copyright = new Label(CommonConstant.COPYRIGHT);
        GridPane.setMargin(copyright,new Insets(10,0,0,0));

        //把组件添加进网格布局中
        gridPane.add(imageView,0,0);
        gridPane.add(version,0,1);
        gridPane.add(copyright,0,2);

        //居中对齐
        setHalignment_CENTER(imageView,version,copyright);

        //把自定义的布局添加到对话框内容中
        dialog.getDialogPane().setContent(gridPane);

        //显示对话框
        dialog.showAndWait();
    }

    /**
     * 组件居中对齐
     * @param nodes 组件
     */
    private void setHalignment_CENTER(Node ...nodes){
        for (Node node:nodes){
            GridPane.setHalignment(node, HPos.CENTER);
        }
    }
}
