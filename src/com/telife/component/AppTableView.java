package com.telife.component;

import com.telife.entity.App;
import com.telife.model.AppModel;
import com.telife.pojo.SharedValue;
import com.telife.util.CommonUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author york
 * @since 2020/6/9
 */
public class AppTableView {

    private TableView tableView;

    private ContextMenu contextMenu;


    public AppTableView() {
        tableView = new TableView();
        tableView.setPrefWidth(750);
        //设置右键菜单
        this.setContextMenu(tableView);
        //右键菜单时，检测是否选中了表格项
        tableView.setOnContextMenuRequested(event -> checkTableItemIsSelected());
        //表格宽度自适应
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //图片列
        TableColumn imgUrlColumn = new TableColumn("图片");
        //将普通文本列设置成图像列
        new TableViewCustomCellFactory().setImageTableColumn(imgUrlColumn);
        imgUrlColumn.setPrefWidth(100);

        //名称列
        TableColumn appNameColumn = new TableColumn("名称");
        appNameColumn.setPrefWidth(100);

        //描述列
        TableColumn appDescColumn = new TableColumn("描述");
        appDescColumn.setPrefWidth(100);

        //访问地址列
        TableColumn appUrlColumn = new TableColumn("访问地址");
        appUrlColumn.setPrefWidth(100);

        setTableColumnCenter(imgUrlColumn,appNameColumn,appDescColumn,appUrlColumn);
        imgUrlColumn.setCellValueFactory(new PropertyValueFactory<>("imgUrl"));
        appNameColumn.setCellValueFactory(new PropertyValueFactory<>("appName"));
        appDescColumn.setCellValueFactory(new PropertyValueFactory<>("appDesc"));
        appUrlColumn.setCellValueFactory(new PropertyValueFactory<>("appUrl"));

        //添加模型数据
        List<AppModel> appModels = new ArrayList<>();
        List<App> appList = CommonUtil.loadDataFromFile();
        appList.forEach(app -> appModels.add(CommonUtil.appToAppModel(app)));
        ObservableList<AppModel> data = FXCollections.observableArrayList(appModels);
        tableView.setItems(data);

        //添加列名
        tableView.getColumns().addAll(imgUrlColumn, appNameColumn, appDescColumn, appUrlColumn);
    }

    public TableView getTableView() {
        return tableView;
    }

    /**
     * 为表格视图设置右键上下文菜单
     *
     * @param tableView
     */
    private void setContextMenu(TableView tableView) {
        contextMenu = new ContextMenu();
        MenuItem addItem = new MenuItem("添加应用");
        MenuItem editItem = new MenuItem("编辑");
        MenuItem deleteItem = new MenuItem("删除");
        contextMenu.getItems().addAll(addItem, editItem, deleteItem);
        this.setMenuItemAction(contextMenu);
        tableView.setContextMenu(contextMenu);
    }

    /**
     * 设置上下文菜单项事件
     *
     * @param contextMenu
     */
    private void setMenuItemAction(ContextMenu contextMenu) {
        MenuItem addItem = contextMenu.getItems().get(0);
        MenuItem editItem = contextMenu.getItems().get(1);
        MenuItem deleteItem = contextMenu.getItems().get(2);

        addItem.setOnAction(event -> insertTableItemOrNot());

        editItem.setOnAction(event -> {
            AppModel appModel = (AppModel) tableView.getSelectionModel().getSelectedItem();
            App app = CommonUtil.appModelToAPP(appModel);
            updateTableItemOrNot(app);
        });

        deleteItem.setOnAction(event -> {
            AppModel appModel = (AppModel) tableView.getSelectionModel().getSelectedItem();
            tableView.getItems().remove(tableView.getSelectionModel().getSelectedIndex());
            //将应用程序图片的地址放入共享变量tempImgPath中
            SharedValue.tempImgPaths.add(appModel.getImgUrl());
            //刷新表格
            tableView.refresh();
            //设置App表格内容可保存
            setSaveable(true);
        });
    }

    /**
     * 检测是否选中表格中的任意一项
     */
    public void checkTableItemIsSelected() {
        //获取当前选中的表格项
        Optional<Object> selectedItem = Optional.ofNullable(tableView.getSelectionModel().getSelectedItem());
        List<MenuItem> menuItems = contextMenu.getItems();
        //如果表格项为空，则设置上下文菜单不可用
        if (!selectedItem.isPresent()) {
            menuItems.forEach(items -> items.setDisable(true));
        } else {
            menuItems.forEach(items -> items.setDisable(false));
        }
        //"添加应用"的菜单项不受表格项影响，在任何情况下都可以使用
        menuItems.get(0).setDisable(false);
    }

    /**
     * 设置App表格视图内容是否可保存
     *
     * @param saveable true代表可保存
     */
    private void setSaveable(boolean saveable) {
        //设置"保存"按钮状态
        ManageAppDialog.setSaveButtonDisable(!saveable);
        //将共享变量中的"app表格视图的编辑状态"
        SharedValue.setAppTableViewEditStatus(saveable);
    }

    /**
     * 新增一行表格项
     */
    private void insertTableItemOrNot() {
        InsertOrUpdateAppDialog dialog = new InsertOrUpdateAppDialog();
        Optional<AppModel> result = dialog.showAndWait();
        if (result.isPresent()) {
            AppModel appModel = result.get();
            //设置应用程序的图片
            String appImg = appModel.getImgUrl().replace("file:","");
            //将图片复制到指定的工程目录
            String newImgSavePath = CommonUtil.copyImageToProject(appImg);
            appModel.setImgUrl(newImgSavePath);
            //将存放的图片位置放入存入变量imgSavePaths中
            SharedValue.imgSavePaths.add(newImgSavePath);
            tableView.getItems().add(appModel);
            //添加一列时，刷新列表，否则可能会导致图像错乱
            tableView.refresh();
            setSaveable(true);
        }
    }

    /**
     * 编辑指定一行表格项
     */
    private void updateTableItemOrNot(App app) {
        InsertOrUpdateAppDialog dialog = new InsertOrUpdateAppDialog();
        Optional<AppModel> result = dialog.showAndWait(app);
        int itemSelectedIndex = tableView.getSelectionModel().getSelectedIndex();
        if (result.isPresent()) {
            AppModel appModel = result.get();
            //设置应用程序的图片
            String appImg = appModel.getImgUrl().replace("file:/","");
            String newImgSavePath = CommonUtil.copyImageToProject(appImg);
            appModel.setImgUrl(newImgSavePath);
            //将存放的图片位置放入存入共享变量imgSavePaths中
            SharedValue.imgSavePaths.add(newImgSavePath);
            tableView.getItems().set(itemSelectedIndex,appModel);
            setSaveable(true);
        }
    }

    /**
     * 表格列内容设置居中
     */
    private void setTableColumnCenter(TableColumn ...tableColumns){
        for(TableColumn tableColumn:tableColumns){
            tableColumn.setStyle("-fx-alignment: CENTER;");
        }
    }

}
