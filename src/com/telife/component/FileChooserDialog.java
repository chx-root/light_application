package com.telife.component;

import com.telife.constant.CommonConstant;
import javafx.stage.FileChooser;

import java.io.File;

/**
 * 文件选择对话框
 * @author york
 * @since 2020/6/10
 */
public class FileChooserDialog {

    private FileChooser fileChooser;

    public FileChooserDialog(){
        // 创建一个文件对话框
        fileChooser = new FileChooser();
    }

    public String showAndWait(){
        // 设置文件对话框的标题
        fileChooser.setTitle("选择文件");

        // 给文件对话框添加多个文件类型的过滤器
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("所有图片", CommonConstant.ACCESS_IMAGE_FORMAT));
        // 显示文件打开对话框
        File file = fileChooser.showOpenDialog(null);
        // 文件对象为空，表示没有选择任何文件
        if (file == null) {
            return null;
        }
        // 文件对象非空，表示选择了某个文件
        else {
            return file.getAbsolutePath();
        }
    }

}
