package com.telife.component;

import cn.hutool.json.JSONUtil;
import com.telife.Main;
import com.telife.entity.App;
import com.telife.model.AppModel;
import com.telife.pojo.SharedValue;
import com.telife.util.CommonUtil;
import com.telife.util.ViewUtil;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author york
 * @since 2020/6/9
 */
public class ManageAppDialog {

    /**
     * "保存"按钮
     */
    private static Node saveButton;

    private AppTableView appTableView;

    public void showDialog(){

        Dialog<Void> dialog = new Dialog<>();
        dialog.initOwner(Main.getStage());
        dialog.setTitle("管理应用");
        dialog.setHeaderText(null);

        //设置按钮
        ButtonType saveButtonType = new ButtonType("保存修改", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancleButtonType =  ButtonType.CANCEL;
        dialog.getDialogPane().getButtonTypes().addAll(saveButtonType, cancleButtonType);

        //默认设置"保存"按钮为不可用
        saveButton = dialog.getDialogPane().lookupButton(saveButtonType);
        setSaveButtonDisable(true);

        //检测点击的按钮
        dialog.setResultConverter(dialogButton -> {
            //如果点击了"保存"按钮，则保存修改
            if (dialogButton == saveButtonType) {
                //保存修改
                saveChanges();
                //设置app表格视图编辑状态
                SharedValue.setAppTableViewEditStatus(false);
            }
            return null;
        });

        //监听"取消"按钮以及右上角的关闭按钮点击事件
        dialog.setOnCloseRequest(event -> {
            //如果保存按钮可用并且表格已经被修改，则说明表格视图已经被编辑过，询问是否保存
            if(!saveButton.isDisabled() && SharedValue.hasEditedAppTableView.get()){
                ConfirmDialog confirmDialog = new ConfirmDialog();
                Optional<ButtonType> result = confirmDialog.showAndWait("提示", "是否保存此次编辑？");
                //点击了确定，则保存修改
                if (result.get() == ButtonType.OK){
                    saveChanges();
                    //清空共享变量里的应用程序图片地址
                    clearAppImgPath();
                }
                //用户取消保存，删除新添加的图片，释放存储空间
                else{
                    deleteAppImages(SharedValue.imgSavePaths);
                    //清空共享变量里的应用程序图片地址
                     clearAppImgPath();
                }

            }
        });

        //自定义内容布局
        VBox vBox = new VBox();

        //设置表格视图
        vBox.getChildren().addAll(getAppTableView().getTableView());

        //设置自定义对话框的显示内容
        dialog.getDialogPane().setContent(vBox);
        dialog.showAndWait();
    }

    /**
     * 设置"保存"按钮是否可用
     * @param disable
     */
    public static void setSaveButtonDisable(boolean disable){
        ViewUtil.setViewDisable(disable,saveButton);
    }

    /**
     * 获取AppTableView对象
     * @return
     */
    private AppTableView getAppTableView(){
        if(appTableView == null){
            appTableView = new AppTableView();
        }
        return appTableView;
    }

    /**
     * 保存对App列表的修改
     */
    private void saveChanges(){
        TableView tableView = getAppTableView().getTableView();
        List<AppModel> items = tableView.getItems();
        List<App> appList = new ArrayList<>();
        //转换把AppModel对象遍历转换成App对象
        items.forEach(item-> appList.add(CommonUtil.appModelToAPP(item)));
        //删除应用程序的旧图片
        deleteAppImages(SharedValue.tempImgPaths);
        //清空共享变量imgSavePaths
        SharedValue.imgSavePaths.clear();
        //把应用程序的数据保存到文件中
        String appData = JSONUtil.toJsonStr(appList);
        CommonUtil.saveDataToFile(appData);
    }

    /**
     * 删除应用程序的图标
     * @param imagePaths
     */
    private void deleteAppImages(List<String> imagePaths){
        for (String imgPath : imagePaths){
            CommonUtil.deleteFile(imgPath);
        }
    }

    /**
     * 清空imgSavePaths和tempImgPaths里的所有元素
     */
    private void clearAppImgPath(){
        //清空集合里的新应用程序图片路径
        SharedValue.imgSavePaths.clear();
        //清空集合里的旧应用程序图片路径
        SharedValue.tempImgPaths.clear();
    }


}
