package com.telife.component;

import com.telife.Main;
import com.telife.constant.CommonConstant;
import com.telife.entity.App;
import com.telife.util.CommonUtil;
import com.telife.util.ViewUtil;
import javafx.concurrent.Worker;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

import java.net.URL;

/**
 * @author york
 * @since 2020/6/8
 */
public class LoadAppDialog {

    public void showDialog(WebView webView) {

        Dialog<Void> dialog = new Dialog<>();
        dialog.initOwner(Main.getStage());
        dialog.initOwner(Main.getStage());
        dialog.setTitle("载入应用");
        dialog.setHeaderText(null);

        //设置按钮
        ButtonType confirmButtonType = new ButtonType("确定", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);

        //默认设置"确定"按钮为不可用
        Node confirmButton = dialog.getDialogPane().lookupButton(confirmButtonType);
        confirmButton.setDisable(true);

        //定义网格布局
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 10, 10, 10));

        //应用标题控件
        Label listTitle = new Label("应用列表");

        //应用列表
        ListView appList = new ListView();
        //自定义应用列表显示内容
        ListViewCustomCellFactory.setCustomCellFactory(appList);

        //添加应用
        appList.getItems().addAll(CommonUtil.loadDataFromFile());

        //垂直布局，用来存放图片控件、应用名称和应用描述信息控件
        VBox vBox = new VBox();
        vBox.setPrefWidth(300);
        vBox.setAlignment(Pos.TOP_CENTER);

        //图像显示控件
        ImageView imageView = new ImageView();
        URL url = getClass().getClassLoader().getResource("resources/icon/appstore.png");
        Image image = new Image(url.toExternalForm());
        imageView.setImage(image);
        imageView.setFitWidth(150);
        imageView.setFitHeight(150);

        //应用名
        Label appName = new Label("请选择应用");

        //应用描述信息
        Label appDesc = new Label(String.format("\u3000%s",""));
        //不要省略超出部分的字体
        appDesc.setWrapText(true);

        //添加组件
        vBox.getChildren().addAll(imageView,appName,appDesc);
        VBox.setMargin(appName,new Insets(20,0,0,0));
        VBox.setMargin(appDesc,new Insets(20,0,0,0));

        //往网格布局里放置控件
        grid.add(listTitle,0,0);
        grid.add(appList,0,1);
        grid.add(vBox,1,1);
        GridPane.setHalignment(listTitle,HPos.CENTER);


        //监听应用列表的点击事件
        appList.setOnMouseClicked(event -> {
            //判断列表项是否被点中
            boolean isNotSelected = null == appList.getSelectionModel().getSelectedItem();
            confirmButton.setDisable(isNotSelected);
            if (!isNotSelected){
                //获取选中的列表信息并且更改相关组件
                App selectedApp = (App) appList.getSelectionModel().getSelectedItem();
                changAppInfoView(selectedApp,imageView,appName,appDesc);
            }
        });

        //点击了确定按钮后，才会加载对应的应用
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == confirmButtonType) {
                App selectedApp = (App) appList.getSelectionModel().getSelectedItem();
                closeCurrentApp(webView);
                loadApp(webView,selectedApp.getAppUrl());
            }
            return null;
        });

        //设置自定义对话框的显示内容
        dialog.getDialogPane().setContent(grid);

        //显示对话框
        dialog.showAndWait();
    }

    /**
     * 更改与App信息相关的组件
     * @param app
     */
    private void changAppInfoView(App app,ImageView imageView,Label appNameLabel, Label appDescLabel){
        Image image = ViewUtil.setImage(app.getImgUrl());;
        imageView.setImage(image);
        appNameLabel.setText(app.getAppName());
        appDescLabel.setText(app.getAppDesc());
    }

    /**
     * 加载App应用
     * @param appUrl 应用地址
     */
    private void loadApp(WebView webView,String appUrl){
        ProcessDialog processDialog = new ProcessDialog();
//        processDialog.showDialog();
        webView.getEngine().load(appUrl);
        Worker worker = webView.getEngine().getLoadWorker();
        processDialog.showDialog(worker.progressProperty());
        worker.stateProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue == Worker.State.SUCCEEDED){
                processDialog.showLoadDoneAndClose(2000);
            }
        });
    }

    /**
     * 关闭当前的app应用
     * @param webView
     */
    private void closeCurrentApp(WebView webView){
        webView.getEngine().load(null);
    }

}
