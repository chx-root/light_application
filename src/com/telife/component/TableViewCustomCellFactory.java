package com.telife.component;

import com.telife.util.ViewUtil;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

/**
 * @author york
 * @since 2020/6/11
 */
public class TableViewCustomCellFactory {

    /**
     * 将表格列设置成图像列
     * @param tableColumn
     */
    public void setImageTableColumn(TableColumn tableColumn){
        tableColumn.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn param) {
                TableCell cell = new TableCell(){
                    @Override
                    protected void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if(item != null && !empty){
                            ImageView imageview = new ImageView();
                            imageview.setFitWidth(100);
                            imageview.setFitHeight(100);
                            imageview.setImage(ViewUtil.setImage(item.toString()));
                            this.setGraphic(imageview);
                        }
                    }
                };
                return cell;
            }
        });
    }

}
