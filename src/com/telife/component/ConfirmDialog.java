package com.telife.component;

import com.telife.Main;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * @author york
 * @since 2020/6/10
 */
public class ConfirmDialog {

    public Optional<ButtonType> showAndWait(String title,String content){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initOwner(Main.getStage());
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        return alert.showAndWait();
    }

}
