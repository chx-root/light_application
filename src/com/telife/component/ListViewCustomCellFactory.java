package com.telife.component;

import com.telife.entity.App;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 * 自定义listView显示内容
 * @author york
 * @since 2020/6/9
 */
public class ListViewCustomCellFactory {

    public static void setCustomCellFactory(ListView listView){
        /*自定义显示对象的指定属性值而不是直接显示对象*/
        listView.setCellFactory(param -> new ListCell<App>(){
            @Override
            protected void updateItem(App item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getAppName() == null) {
                    setText(null);
                } else {
                    setText(item.getAppName());
                }
            }
        });
    }


}
