package com.telife.constant;

import com.telife.entity.App;

import java.util.ArrayList;
import java.util.List;

/**
 * @author york
 * @since 2020/6/9
 */
public class CommonConstant {
    public static List<App> appList = new ArrayList<App>() {{
        add(new App("百度", "好玩的应用1", "file:C:\\Users\\York\\Desktop\\test\\timg(2).jpg", "https://www.baidu.com/"));
        add(new App("奶牛快传", "好玩的应用2", "file:C:\\Users\\York\\Desktop\\test\\timg(3).jpg", "https://cowtransfer.com"));
        add(new App("B站", "好玩的应用3", "file:C:\\Users\\York\\Desktop\\test\\timg.jpg", "http://www.bilibili.com/"));
    }};

    /**
     * 可接受的图像格式
     */
    public static final String[] ACCESS_IMAGE_FORMAT = new String[]{"*.jpg", "*.png"};

    /**
     * 存放应用程序的文件目录
     */
    public static final String DATA_FILE = System.getProperty("user.dir") + "\\data";

    /**
     * 存放应用程序图标的文件目录
     */
    public static final String APP_IMAGE_FILE = DATA_FILE + "\\app_img\\";

    /**
     * 存放应用程序数据的文件路径
     */
    public static final String APP_DATA_FILE_PATH = DATA_FILE + "\\app.data";

    /**
     * 版权信息
     */
    public final static String COPYRIGHT = "Copyright © 2020 York. All rights reserved.";

    /**
     * 版本信息
     */
    public final static String VERSION = "V1.0.0 / 2020.06.12";

    /**
     * Logo图标地址
     */
    public final static String LOGO = "resources/icon/logo.png";

}
