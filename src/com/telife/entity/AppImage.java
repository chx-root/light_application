package com.telife.entity;

/**
 * @author york
 * @since 2020/6/11
 */
public class AppImage {

    /**
     * 图片地址
     */
    private String imgUrl;

    /**
     * 图示是应用已存在的还是最新的（用于区分新增应用和编辑应用）
     */
    private Boolean existed;

    public AppImage(String imgUrl, Boolean existed) {
        this.imgUrl = imgUrl;
        this.existed = existed;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Boolean getExisted() {
        return existed;
    }

    public void setExisted(Boolean existed) {
        this.existed = existed;
    }

    @Override
    public String toString() {
        return "AppImage{" +
                "imgUrl='" + imgUrl + '\'' +
                ", existed=" + existed +
                '}';
    }
}
