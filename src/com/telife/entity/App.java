package com.telife.entity;

/**
 * @author york
 * @since 2020/6/9
 */
public class App {
    /**
     * 应用名称
     */
    private String appName;
    /**
     * 应用描述
     */
    private String appDesc;

    /**
     * 应用图标
     */
    private String imgUrl;

    /**
     * App应用地址
     */
    private String appUrl;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppDesc() {
        return appDesc;
    }

    public void setAppDesc(String appDesc) {
        this.appDesc = appDesc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public App(){}

    public App(String appName, String appDesc, String imgUrl, String appUrl) {
        this.appName = appName;
        this.appDesc = appDesc;
        this.imgUrl = imgUrl;
        this.appUrl = appUrl;
    }

    @Override
    public String toString() {
        return "App{" +
                "appName='" + appName + '\'' +
                ", appDesc='" + appDesc + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", appUrl='" + appUrl + '\'' +
                '}';
    }
}
